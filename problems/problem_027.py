# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None
    return max(values)


test = [1,2,3,4,5,67,6]

print(max_in_list(test))
