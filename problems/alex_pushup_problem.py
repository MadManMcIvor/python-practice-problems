def alex_mistakes(katas, limit):
    time_for_mistakes = limit  - (katas * 6)
    counter = 0
    while time_for_mistakes - (5 * (2 ** (counter))) >= 0:
        time_for_mistakes = time_for_mistakes - 5 * (2 ** counter)
        counter += 1 
    return counter

print(alex_mistakes(10,120))
print(alex_mistakes(11,120))
print(alex_mistakes(3,45))
print(alex_mistakes(8,120))
print(alex_mistakes(6,60))
print(alex_mistakes(20,120))


