# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function



def biggest_gap(list):
    if len(list) < 2:
        return None
    temp = sorted(list)
    result = temp[len(temp)-1] - temp[0]
    return result

test = [1,4,7,20,100]
test2 = [1,4,7,20,-100]
test3= [1]
test4 = []
test5 = [-100, -20, -40]

print(biggest_gap(test))
print(biggest_gap(test2))
print(biggest_gap(test3))
print(biggest_gap(test4))
print(biggest_gap(test5))