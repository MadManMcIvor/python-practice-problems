# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def can_make_pasta(ingredients):
#     list = ["flour", "eggs", "oil"]
#     counter = 0
#     print(list)
#     for item in ingredients:
#         if item in list:
#             counter += 1
#             print(item)
#     if counter == 3:
#         return True
#     else:
#         return False


def can_make_pasta(ingredients):
    list = ["flour", "eggs", "oil"]
    if ingredients[0] in list and ingredients[1] in list and ingredients[2] in list:
        return True
    else:
        return False


test = ["flour", "eggs", "oil"]
print(can_make_pasta(test))
