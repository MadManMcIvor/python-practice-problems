# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if len(values) == 0:
        return None
    return sum(values)

test = [1,2,3,4,5,6,7,8,9,11]
test1 = []

print(calculate_sum(test))
print(calculate_sum(test1))  
