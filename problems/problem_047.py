# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lower = 0
    upper = 0
    digit = 0
    special = 0
    if len(password) < 6 or len(password) > 12:
        return "Password needs to be between 6 and 12 characters"
    for char in password:
        if char.islower():
            lower += 1
        if char.isupper():
            upper += 1
        if char.isdigit():
            digit += 1
        if char in ['$', '!', '@']:
            special += 1
    if special == 0:
        return "You failed! The password doesn't work, you big dummy!"
    if upper == 0:
        return "You failed! The password doesn't work, you big dummy!"
    if lower == 0:
        return "You failed! The password doesn't work, you big dummy!"
    if  digit == 0:
        return "You failed! The password doesn't work, you big dummy!"
    return "Great Password"
        

print(check_password("porkchop"))