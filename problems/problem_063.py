# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(string):
    result = ""
    for char in string:
        if ord(char) >= 65 and ord(char) <= 90:
            result += chr(((ord(char)-65 + 1) % 26) + 65)
        if ord(char) >= 97 and ord(char) <= 122:
            result += chr(((ord(char)-97 + 1) % 26) + 97)
    return result

print(shift_letters("import"))
print(shift_letters("IMPORTZ"))