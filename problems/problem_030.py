# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#account for the none statements
#then sort given list
#return value index second from the highest from sorted list


def find_second_largest(values):
    if len(values) == 0 or len(values) == 1:
        return None
    values.sort(reverse=True)
    return values[1]


test = [1,5,6,77,7,777,8,10]

print(find_second_largest(test))
    
