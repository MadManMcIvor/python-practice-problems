#Link:  https://www.codewars.com/kata/54d81488b981293527000c8f/train/python

#first attempts - works but times out on the larger test
# def sum_pairs(ints, s):
#     result_pairs = []
#     result_indicies = []
#     for i in range(len(ints)):
#         for j in range(1 + i, len(ints)):
#             if ints[i] + ints[j] == s:
#                 pair = [ints[i], ints[j]]
#                 result_pairs.append(pair)
#                 result_indicies.append(j)
#     if len(result_pairs) == 0:
#         return None
#     if len(result_pairs) == 1:
#         return result_pairs[0]
#     if len(result_pairs) > 1:
#         lowvalue = result_pairs[0]
#         lowindex = 0
#         for i in range(len(result_pairs)):
#             if result_pairs[i] < lowvalue:
#                 lowvalue = result_pairs[i]
#                 lowindex = i
#         return result_pairs[lowindex]
    

# print(sum_pairs([4, 3, 2, 3, 4], 6))
# print(sum_pairs([10, 5, 2, 3, 7, 5], 10))


#attempt 2: better but still timed out
# def sum_pairs(ints, s):
#     result_pairs = []
#     result_index = len(ints) + 1
#     for i in range(len(ints)):
#         for j in range(1 + i, len(ints)):
#             if ints[i] + ints[j] == s:
#                 pair = [ints[i], ints[j]]
#                 if j < result_index:
#                     result_index = j
#                     result_pairs = pair
#     if len(result_pairs) == 0:
#         return None
#     return result_pairs
    

# print(sum_pairs([4, 3, 2, 3, 4], 6))
# print(sum_pairs([10, 5, 2, 3, 7, 5], 10))

#Attempt 3: 
# https://www.geeksforgeeks.org/python-all-possible-pairs-in-list/
# def sum_pairs(ints, s):
#     if len(ints) == 0:
#         return None
#     res = [(a, b) for idx, a in enumerate(ints) for b in ints[idx + 1:] if a + b == s]
#     print(res)
    # result = []
    # index = len(ints) + 1
    # for pair in res:
    #     if ints.index(pair[1]) < index:
    #         index = ints.index(pair[1])
    #         result = pair
    # return result


# print(sum_pairs([4, 3, 2, 3, 4], 6))
# print(sum_pairs([10, 5, 2, 3, 7, 5], 10))


#I gave up. Here's the best answer from CodeWars:
# def sum_pairs(lst, s):
#     cache = set()
#     for i in lst:
#         if s - i in cache:
#             return [s - i, i]
#         cache.add(i)