# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

from unittest import result


def sum_of_squares(values):
    result = 0
    if values == []:
        return None
    for item in values:
        result = result + (item * item)
    return result

test = [1,5,9]
test1 = [-4, -1, 0]
test2 = []

print(sum_of_squares(test))
print(sum_of_squares(test1))
print(sum_of_squares(test2))